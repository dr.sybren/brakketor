package stackdetect

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of Brakketor.
 *
 * Brakketor is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Brakketor is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Brakketor.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"context"
	"errors"
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"
	"sync"

	"github.com/rwcarlsen/goexif/exif"
)

type StackDetector struct {
	FoundExposures map[string]bool // Set of stringified photo exposure info
	Stacks         []Stack         // Slice of image stacks

	ctx       context.Context
	ouputSync sync.Locker
	messages  chan string // Messages for the user are sent here, and should be consumed by the GUI.
}

func New(ctx context.Context) StackDetector {
	sd := StackDetector{
		FoundExposures: make(map[string]bool),
		Stacks:         make([]Stack, 0),

		ctx:       ctx,
		ouputSync: new(sync.Mutex),
		messages:  make(chan string),
	}

	return sd
}

// Analyze inspects images and subdivides them into bracket stacks.
func (sd *StackDetector) Analyze(glob_path string) error {
	if glob_path == "" {
		return errors.New("no glob path given")
	}
	if !strings.Contains(glob_path, "*") {
		glob_path = filepath.Join(glob_path, "*")
	}

	filenames, err := filepath.Glob(glob_path)
	if err != nil {
		return fmt.Errorf("invalid glob pattern: %v", err)
	}
	sd.log("found %d files", len(filenames))

	stackExposures := make(map[string]bool)
	stack := make(Stack, 0)

	for _, filename := range filenames {
		stat, err := os.Stat(filename)
		if err != nil {
			return fmt.Errorf("could not check file %v: %v", filename, err)
		}
		if stat.IsDir() {
			continue
		}

		// sd.log("analyzing %v", filename)
		exposure, err := sd.findExposure(filename)
		if err != nil {
			return fmt.Errorf("could not determine exposure of %v: %v", filename, err)
		}

		sd.log("%v = %v", path.Base(filename), exposure)

		sd.FoundExposures[exposure] = true
		if stackExposures[exposure] {
			// Seen this exposure before; start a new stack.
			sd.Stacks = append(sd.Stacks, stack)
			stackExposures = make(map[string]bool)
			stack = make(Stack, 0)
		}

		stackExposures[exposure] = true
		stack = append(stack, filename)
	}
	sd.Stacks = append(sd.Stacks, stack)
	return nil
}

func (sd *StackDetector) findExposure(filename string) (string, error) {
	// data, err := ioutil.ReadFile(filename)
	// if err != nil {
	// 	return "", fmt.Errorf("error reading %v: %v", filename, err)
	// }

	// // TODO: check filenaem & fail if not JPEG.
	// // TODO: support non-JPEG files.
	// mediaParser := jpegstructure.NewJpegMediaParser()
	// intfc, err := mediaParser.ParseBytes(data)
	// if err != nil {
	// 	return "", fmt.Errorf("error parsing %v: %v", filename, err)
	// }

	// sl := intfc.(*jpegstructure.SegmentList)
	// _, _, et, err := sl.DumpExif()
	// if err != nil {
	// 	return "", fmt.Errorf("error getting EXIF from %v: %v", filename, err)
	// }
	// if len(et) == 0 {
	// 	return "", fmt.Errorf("empty EXIF in %v", filename)
	// }

	imgFile, err := os.Open(filename)
	if err != nil {
		return "", fmt.Errorf("opening: %v", err)
	}

	metaData, err := exif.Decode(imgFile)
	if err != nil {
		return "", fmt.Errorf("parsing: %v", err)
	}

	exposureTime, err := metaData.Get(exif.ExposureTime)
	if err != nil {
		return "", fmt.Errorf("getting exposureTime value of: %v", err)
	}
	fNumber, err := metaData.Get(exif.FNumber)
	if err != nil {
		return "", fmt.Errorf("getting fNumber value of: %v", err)
	}
	iso, err := metaData.Get(exif.ISOSpeedRatings)
	if err != nil {
		return "", fmt.Errorf("getting ISO value of: %v", err)
	}

	exposure := fmt.Sprintf("%v-%v-%v",
		exposureTime.String(),
		fNumber.String(),
		iso.String(),
	)

	return exposure, nil
}

//  def merge_stack(self, input_paths: list[Path], outfname: Path) -> None:
//      try:
//          self._print("\t".join(str(p) for p in input_paths))

//          cmd = (
//              str(ENFUSE),
//              f"--output={outfname}",
//              "--compression=deflate",
//              "--depth=16",
//              *[str(p) for p in input_paths],
//          )
//          proc = subprocess.run(cmd, check=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
//      except BaseException as ex:
//          self._print(f"ERROR {ex} running {cmd}")
//          self._abort.set()
//          return

//      if proc.returncode == 0:
//          return

//      self._print(f"ERROR {proc.returncode} running {cmd}:\n{proc.stdout}")
//      self._abort.set()

func (sd *StackDetector) log(format string, args ...interface{}) {
	sd.messages <- fmt.Sprintf(format, args...)
}

func (sd *StackDetector) Messages() <-chan string {
	return sd.messages
}

func (sd *StackDetector) Close() {
	close(sd.messages)
}
