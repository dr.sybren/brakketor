package main

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of Brakketor.
 *
 * Brakketor is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Brakketor is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Brakketor.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"context"
	"flag"
	"fmt"
	"os"
	"time"

	"fyne.io/fyne/v2/app"

	"gitlab.com/dr.sybren/brakketor/appinfo"
	"gitlab.com/dr.sybren/brakketor/gui"
)

var cliArgs struct {
	version bool

	inputGlob string
}

func parseCliArgs() {
	flag.BoolVar(&cliArgs.version, "version", false, "Show version number and quit.")
	flag.Parse()

	switch {
	case flag.NArg() > 1:
		flag.Usage()
		fmt.Println("  file glob")
		fmt.Println("        Optional input file search pattern")
		os.Exit(1)
	case flag.NArg() == 1:
		cliArgs.inputGlob = flag.Arg(0)
	}
}

func main() {
	parseCliArgs()
	if cliArgs.version {
		fmt.Printf("%s %s\n", appinfo.ApplicationName, appinfo.ApplicationVersion)
		return
	}

	ctx, cancelFunc := context.WithCancel(context.Background())

	a := app.NewWithID("eu.stuvel.brakketor")
	w := gui.NewWindow(ctx, a)
	if len(cliArgs.inputGlob) != 0 {
		w.SetInputGlob(cliArgs.inputGlob)
	} else {
		w.SetInputGlob(a.Preferences().String("inputGlob"))
	}
	w.SetOutputPathFromInputGlob()
	w.ShowAndRun()

	cancelFunc()

	a.Preferences().SetString("inputGlob", w.GetInputGlob())

	// TODO: actually have a good way to wait for background processes to get nuked.
	time.Sleep(250 * time.Millisecond)
}
