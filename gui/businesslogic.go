package gui

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of Brakketor.
 *
 * Brakketor is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Brakketor is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Brakketor.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"context"
	"os"
	"strings"
	"sync"

	"gitlab.com/dr.sybren/brakketor/enfuser"
	"gitlab.com/dr.sybren/brakketor/priority"
	"gitlab.com/dr.sybren/brakketor/stackdetect"
)

func (mw *MainWindow) Analyze(inputGlob string) error {
	inputGlob = strings.TrimSpace(inputGlob)
	mw.logInfoMessage("Analyzing %#v\n", inputGlob)

	mw.newStackDetector()
	err := mw.stackdet.Analyze(inputGlob)
	if err != nil {
		return err
	}

	mw.logInfoMessage(
		"Found %v stacks of %v exposures",
		len(mw.stackdet.Stacks), len(mw.stackdet.FoundExposures))
	return nil
}

func (mw *MainWindow) Merge(outputDir string) error {
	err := mergeInBackground(mw.ctx, mw.stackdet.Stacks, outputDir)
	if err != nil {
		return err
	}

	mw.logInfoMessage("Merge done!")
	return nil
}

func (mw *MainWindow) newStackDetector() {
	// Properly close pre-existing stack detector.
	if mw.stackdet != nil {
		mw.stackdet.Close()
	}

	sd := stackdetect.New(mw.ctx)
	mw.stackdet = &sd

	go func() {
		for msg := range sd.Messages() {
			mw.logMessage(levelProgress, msg)
		}
	}()
}

func mergeInBackground(ctx context.Context, stacks []stackdetect.Stack, outputDir string) error {
	if err := priority.BackgroundStart(); err != nil {
		return err
	}
	defer priority.BackgroundEnd()
	return merge(ctx, stacks, outputDir)
}

func merge(ctx context.Context, stacks []stackdetect.Stack, outputDir string) error {
	err := os.MkdirAll(outputDir, os.ModePerm)
	if err != nil {
		return err
	}

	// Make a pool of workers
	numWorkers := 6
	wg := new(sync.WaitGroup)
	wg.Add(numWorkers)
	stackChan := make(chan stackdetect.Stack)

	// Start the workers
	for i := 0; i < numWorkers; i++ {
		worker := enfuser.New(ctx, i, outputDir)
		go worker.Work(wg, stackChan)
	}

	// Feed the workers
	for _, stack := range stacks {
		stackChan <- stack
	}
	close(stackChan)

	// Wait until the work is done
	wg.Wait()
	return nil
}
