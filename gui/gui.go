package gui

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of Brakketor.
 *
 * Brakketor is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Brakketor is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Brakketor.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"context"
	"path/filepath"
	"strings"
	"time"

	fyne "fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/widget"

	"gitlab.com/dr.sybren/brakketor/appinfo"
	"gitlab.com/dr.sybren/brakketor/stackdetect"
)

type MainWindow struct {
	win      fyne.Window
	ctx      context.Context
	stackdet *stackdetect.StackDetector

	// Input & output sections
	inputGlob       binding.String
	outputDir       binding.String
	inputSelectBox  *fyne.Container
	outputSelectBox *fyne.Container

	// Messages/logging section
	statusBarLabel  *widget.Label
	lastStatusLevel MessageLevel
}

type MessageLevel int

const (
	levelProgress = iota
	levelInfo
	levelError
)

func NewWindow(ctx context.Context, app fyne.App) MainWindow {
	fyneWindow := app.NewWindow(appinfo.FormattedApplicationInfo())
	fyneWindow.Resize(fyne.NewSize(800, 200))

	mw := MainWindow{
		ctx: ctx,
		win: fyneWindow,

		inputGlob: binding.NewString(),
		outputDir: binding.NewString(),
	}

	// Input file selector.
	inputLabel := widget.NewLabel("Input:")
	inputGlobEntry := widget.NewEntryWithData(mw.inputGlob)
	inputGlobEntry.Validator = inputGlobValidator
	inputGlobEntry.PlaceHolder = "Input file pattern"
	go func() {
		time.Sleep(500 * time.Millisecond)
		inputGlobEntry.Validate()
	}()

	analyzeButton := widget.NewButton("Analyze", mw.onButtonAnalyze)
	mw.inputSelectBox = container.NewWithoutLayout(
		inputLabel, inputGlobEntry, analyzeButton,
	)

	// Output file selector.
	outputLabel := widget.NewLabel("Output:")
	outputEntry := widget.NewEntryWithData(mw.outputDir)
	mergeButton := widget.NewButton("Merge", mw.onButtonMerge)
	mw.outputSelectBox = container.NewWithoutLayout(
		outputLabel, outputEntry, mergeButton,
	)
	disableContainer(mw.outputSelectBox)

	formContainer := container.New(
		NewForm3Layout(),
		inputLabel, inputGlobEntry, analyzeButton,
		outputLabel, outputEntry, mergeButton,
	)

	// Messages/logging section
	mw.statusBarLabel = widget.NewLabel("")

	fyneWindow.SetContent(container.NewVBox(
		formContainer,
		widget.NewSeparator(),
		mw.statusBarLabel,
	))

	return mw
}

func (mw *MainWindow) ShowAndRun() {
	mw.win.ShowAndRun()
}

func (mw *MainWindow) SetInputGlob(glob string) {
	mw.inputGlob.Set(strings.TrimSpace(glob))
}

func (mw *MainWindow) GetInputGlob() string {
	glob, _ := mw.inputGlob.Get()
	return glob
}

func (mw *MainWindow) SetOutputPath(directory string) {
	mw.outputDir.Set(strings.TrimSpace(directory))
}

func (mw *MainWindow) SetOutputPathFromInputGlob() {
	inputGlob, _ := mw.inputGlob.Get()
	if inputGlob == "" {
		mw.SetOutputPath("")
		return
	}
	inputDir := inputGlob
	if strings.Contains(inputDir, "*") {
		inputDir = filepath.Dir(inputDir)
	}
	output := filepath.Join(filepath.Dir(inputDir), "hdr-merged")
	mw.SetOutputPath(output)
}

func (mw *MainWindow) onButtonAnalyze() {
	mw.logReset()
	disableContainer(mw.inputSelectBox)
	disableContainer(mw.outputSelectBox)
	inputGlob, _ := mw.inputGlob.Get()

	go func() {
		defer enableContainer(mw.inputSelectBox)
		err := mw.Analyze(inputGlob)
		if err != nil {
			mw.logErrorMessage("Error analyzing files: %v", err)
			return
		}

		enableContainer(mw.outputSelectBox)
		mw.SetOutputPathFromInputGlob()
	}()
}

func (mw *MainWindow) onButtonMerge() {
	mw.logReset()
	outputDir, _ := mw.outputDir.Get()
	if len(outputDir) == 0 {
		mw.logErrorMessage("No output configured")
		return
	}

	disableContainer(mw.inputSelectBox)
	disableContainer(mw.outputSelectBox)

	go func() {
		defer enableContainer(mw.inputSelectBox)
		defer enableContainer(mw.outputSelectBox)

		err := mw.Merge(outputDir)
		if err != nil {
			mw.logErrorMessage("Error merging files: %v", err)
			return
		}
	}()
}
