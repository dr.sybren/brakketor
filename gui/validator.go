package gui

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of Brakketor.
 *
 * Brakketor is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Brakketor is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Brakketor.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func inputGlobValidator(input string) error {
	err := actualInputGlobValidator(input)
	fmt.Printf("Validation result: %v\n", err)
	return err
}

func actualInputGlobValidator(input string) error {
	if strings.TrimSpace(input) == "" {
		return errors.New("input empty")
	}

	if _, err := os.Stat(input); err != os.ErrNotExist {
		// err is either nil (path exists) or indicates an access error.
		// Either should be reported to the user.
		return err
	}

	dir, file := filepath.Split(input)
	if strings.Contains(dir, "*") {
		return errors.New("directory cannot contain glob")
	}
	if !strings.Contains(file, "*") {
		// No glob, but also no exist.
		return errors.New("input does not exist")
	}

	if _, err := os.Stat(dir); err != os.ErrNotExist {
		// err is either nil (path exists) or indicates an access error.
		// Either should be reported to the user.
		return err
	}

	return errors.New("input does not exist")
}
