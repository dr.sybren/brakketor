package gui

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of Brakketor.
 *
 * Brakketor is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Brakketor is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Brakketor.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import "fyne.io/fyne/v2"

func enableContainer(container *fyne.Container) {
	for _, canvasObject := range container.Objects {
		if disableable, ok := canvasObject.(fyne.Disableable); ok {
			disableable.Enable()
		}
	}
}

func disableContainer(container *fyne.Container) {
	for _, canvasObject := range container.Objects {
		if disableable, ok := canvasObject.(fyne.Disableable); ok {
			disableable.Disable()
		}
	}
}
