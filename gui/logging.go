package gui

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of Brakketor.
 *
 * Brakketor is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Brakketor is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Brakketor.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import "fmt"

func (mw *MainWindow) logMessage(level MessageLevel, message string) {
	// TODO: use logging framework for this.
	fmt.Println(message)

	if mw.lastStatusLevel <= level {
		mw.lastStatusLevel = level
		mw.statusBarLabel.SetText(message)
	}
}

func (mw *MainWindow) logInfo(message string) {
	mw.logMessage(levelInfo, message)
}

func (mw *MainWindow) logInfoMessage(format string, args ...interface{}) {
	message := fmt.Sprintf(format, args...)
	mw.logInfo(message)
}

func (mw *MainWindow) logError(err error) {
	mw.logErrorMessage(err.Error())
}

func (mw *MainWindow) logErrorMessage(format string, args ...interface{}) {
	message := fmt.Sprintf(format, args...)
	mw.logMessage(levelError, message)
}

func (mw *MainWindow) logReset() {
	mw.lastStatusLevel = 0
	mw.statusBarLabel.SetText("")
}
