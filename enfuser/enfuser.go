package enfuser

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of Brakketor.
 *
 * Brakketor is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Brakketor is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Brakketor.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"os/exec"
	"path/filepath"
	"sync"
	"time"

	"gitlab.com/dr.sybren/brakketor/stackdetect"
)

const enfusePath = "C:\\Program Files\\Hugin\\bin\\enfuse.exe"

type Enfuser struct {
	ctx       context.Context
	idx       int
	outputDir string
	messages  chan string
}

func New(ctx context.Context, workerIndex int, outputDir string) Enfuser {
	return Enfuser{
		ctx, workerIndex, outputDir, make(chan string),
	}
}

func (e *Enfuser) Work(wg *sync.WaitGroup, stackChannel <-chan stackdetect.Stack) {
	defer wg.Done()

	// Sleep for a random time before starting a job to prevent all parallel jobs
	// from starting simultaneously.
	e.randomSleep()

	// TODO: have a way to communicate errors
	for {
		select {
		case <-e.ctx.Done():
			fmt.Printf("worker[%d] app is closed\n", e.idx)
			return
		case stack, ok := <-stackChannel:
			if !ok {
				// Normal case: all stacks have been merged.
				fmt.Printf("worker[%d] work is done\n", e.idx)
				return
			}
			e.mergeStack(stack)
		}
	}
}

func (e *Enfuser) randomSleep() {
	duration := rand.Int31n(1000) + 500
	time.Sleep(time.Duration(duration) * time.Millisecond)
}

func (e *Enfuser) mergeStack(stack stackdetect.Stack) error {
	outputFname := stack.SuitableOutputName(".tif")
	outputPath := filepath.Join(e.outputDir, outputFname)

	if _, err := os.Stat(outputPath); err == nil {
		fmt.Printf("%v exists, skipping\n", outputFname)
		return nil
	}

	args := []string{
		"--output=" + outputPath,
		"--compression=deflate",
		"--depth=16",
	}
	args = append(args, stack...)
	fmt.Printf("cmd: %v\n", args)
	// mw.logInfo(outputFname)

	cmd := exec.CommandContext(e.ctx, enfusePath, args...)
	// TODO: call cmd.CombinedOutput() instead, and put any failure output in the GUI.
	err := cmd.Run()
	if err != nil {
		// Remove any partially-written image.
		os.Remove(outputPath)
		return err
	}
	return nil
}
