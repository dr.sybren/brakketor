package priority

// BackgroundStart sets the current process' CPU priority to something lower than normal.
func BackgroundStart() error {
	return bgStart()
}

// BackgroundEnd returns the current process' CPU priority to normal.
func BackgroundEnd() error {
	return bgEnd()
}
