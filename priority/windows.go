//go:build windows

package priority

import (
	"fmt"
	"syscall"
)

func bgStart() error {
	return setPriorityClass(PROCESS_MODE_BACKGROUND_BEGIN)
}

func bgEnd() error {
	return setPriorityClass(PROCESS_MODE_BACKGROUND_END)
}

func setPriorityClass(priorityClass uintptr) error {
	kernel32, err := kernel32Load()
	if err != nil {
		return err
	}
	defer kernel32Free(kernel32)

	getCurrentProcess, err := kernel32Function(kernel32, "GetCurrentProcess")
	if err != nil {
		return err
	}

	currentProcess, _, errno := syscall.SyscallN(getCurrentProcess)
	if errno != 0 {
		return fmt.Errorf("error calling GetCurrentProcess from kernel32.nll: %d; %w", errno, errno)
	}

	// // https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/nf-processthreadsapi-setpriorityclass
	setPriorityClass, err := kernel32Function(kernel32, "SetPriorityClass")
	if err != nil {
		return err
	}

	result, _, errno := syscall.SyscallN(setPriorityClass, currentProcess, priorityClass)
	if errno != 0 {
		return fmt.Errorf("error calling SetPriorityClass from kernel32.nll: %d; %w", errno, errno)
	}
	if result == 0 {
		getLastError, err := kernel32Function(kernel32, "GetLastError")
		if err != nil {
			return fmt.Errorf("SetPriorityClass call failed, but could not load GetLastError to find the cause: %w", err)
		}
		errorCode, _, errno := syscall.SyscallN(getLastError)
		if errno != 0 {
			return fmt.Errorf("SetPriorityClass call failed, but could not call GetLastError to find the cause: %w", errno)
		}
		return fmt.Errorf("SetPriorityClass call failed, error code: %v", errorCode)
	}

	return nil
}

func kernel32Load() (syscall.Handle, error) {
	libname := "kernel32.dll"
	kernel32, err := syscall.LoadLibrary(libname)
	if err != nil {
		return 0, fmt.Errorf("loading %s: %w", libname, err)
	}
	return kernel32, nil
}

func kernel32Free(kernel32 syscall.Handle) {
	_ = syscall.FreeLibrary(kernel32)
}

func kernel32Function(kernel32 syscall.Handle, funcname string) (uintptr, error) {
	procAddress, err := syscall.GetProcAddress(kernel32, funcname)
	if err != nil {
		return 0, fmt.Errorf("finding function %s in kernel32.dll: %w", funcname, err)
	}
	return procAddress, nil
}

// Source: https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/nf-processthreadsapi-setpriorityclass
const (
	// Process that has priority above NORMAL_PRIORITY_CLASS but below
	// HIGH_PRIORITY_CLASS.
	ABOVE_NORMAL_PRIORITY_CLASS = 0x00008000

	// Process that has priority above IDLE_PRIORITY_CLASS but below
	// NORMAL_PRIORITY_CLASS.
	BELOW_NORMAL_PRIORITY_CLASS = 0x00004000

	// Process that performs time-critical tasks that must be executed
	// immediately. The threads of the process preempt the threads of normal or
	// idle priority class processes. An example is the Task List, which must
	// respond quickly when called by the user, regardless of the load on the
	// operating system. Use extreme care when using the high-priority class,
	// because a high-priority class application can use nearly all available CPU
	// time.
	HIGH_PRIORITY_CLASS = 0x00000080

	// Process whose threads run only when the system is idle. The threads of the
	// process are preempted by the threads of any process running in a higher
	// priority class. An example is a screen saver. The idle-priority class is
	// inherited by child processes.
	IDLE_PRIORITY_CLASS = 0x00000040

	// Process with no special scheduling needs.
	NORMAL_PRIORITY_CLASS = 0x00000020

	// Begin background processing mode. The system lowers the resource scheduling
	// priorities of the process (and its threads) so that it can perform
	// background work without significantly affecting activity in the foreground.
	//
	// This value can be specified only if hProcess is a handle to the current
	// process. The function fails if the process is already in background
	// processing mode.
	PROCESS_MODE_BACKGROUND_BEGIN = 0x00100000

	// End background processing mode. The system restores the resource scheduling
	// priorities of the process (and its threads) as they were before the process
	// entered background processing mode.
	//
	// This value can be specified only if hProcess is a handle to the current
	// process. The function fails if the process is not in background processing
	// mode.
	PROCESS_MODE_BACKGROUND_END = 0x00200000

	// Process that has the highest possible priority. The threads of the process
	// preempt the threads of all other processes, including operating system
	// processes performing important tasks. For example, a real-time process that
	// executes for more than a very brief interval can cause disk caches not to
	// flush or cause the mouse to be unresponsive.
	REALTIME_PRIORITY_CLASS = 0x00000100
)
